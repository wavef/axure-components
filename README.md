# Axure组件集合

### 概述
本仓库主要集合了使用JSBox组件制作的Axure组件，你无需具备任何代码知识，开箱即用，只需要简单在JSBox中继器上修改配置即可实现各种高级交互功能，让你在高保真原型的路上得心应手，搞定上级要求，拿下各种难缠甲方。

### 使用方法
下载rp文件，用Axure RP 9.x 或更高版本打开，调整中继器的配置项，或根据相关提示添加素材，点击预览查看效果。

### JSBox介绍
JSBox是一款面向全栈交互设计师的Axure组件，让你在Axure中轻松制作高保真原型。JSBOX可以通过编写JS实现三维、动画等交互，同时也能用来制作零代码组件，提供给其他不懂代码的用户使用。

- 下载地址：https://axured.cn/a/1143.html

- 文档地址：https://gitee.com/wavef/axlib


### 组件截图
![地图组件](https://ax.minicg.com/images/jsbox/jsbox-preview-01.gif)
![签名组件](https://ax.minicg.com/images/jsbox/jsbox-preview-02.gif)
![轮播组件](https://ax.minicg.com/images/jsbox/jsbox-preview-03.gif)


### 参与贡献

- [@testdevops](https://gitee.com/testdevops)
- [@wavef](https://gitee.com/wavef)

