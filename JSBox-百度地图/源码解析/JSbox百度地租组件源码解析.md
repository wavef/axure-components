```
// JSBOX v2.x

const { log, dir, clear } = console
//获取秘钥和样式资源请查看文档说明https://lbsyun.baidu.com/index.php?title=jspopularGL/guide/custom
// 对接中继器数据
const config = THIS.getData({ format: "row" })
const apikey = config["地图密钥"][0]
const MapStyleId = config["样式id"][0]
const zoom = config["缩放级别"][0]
const latlon = config["中心经纬度"][0].replace(" ", "").split(",")
const rotate = Number(config["旋转角度"][0])
const tilt = Number(config["倾斜角度"][0])
const logo = config["隐藏Logo"][0] === 0

// 外挂回调函数（百度要求）
window.initBaiduMap = initBaiduMap

// 加载地图SDK
Loader(
  `https://api.map.baidu.com/api?type=webgl&v=1.0&ak=${apikey}&callback=initBaiduMap`
)

// 初始化地图
async function initBaiduMap() {
  const map = new BMapGL.Map(THIS.vid)
  map.centerAndZoom(new BMapGL.Point(...latlon), zoom)
  map.enableScrollWheelZoom(true)
  debugger
  if (MapStyleId !== undefined) {
    // 设置地图样式by用户设定
    map.setMapStyleV2({
      styleId: MapStyleId
    })
  } else {
  //此处默认的样式是获取外网的服务器数据
    const bin = new jsonbin("64acf45a9d312622a37d98dd")
    const res = await bin.read()
    map.setMapStyleV2({styleJson: res.record .mapStyle});
  }
  // 设置旋转与倾斜角度
  map.setHeading(rotate)
  map.setTilt(tilt)

  if (!logo) {
    // 隐藏logo
    $(".anchorBL").hide()
  }
}
```