

-  第一步  注册百度账号和申请百度地图秘钥ak  移步[这看教程](https://lbsyun.baidu.com/index.php?title=jspopularGL/guide/getkey)
-  第二步  创建自己的样式文件 并发布 [仔细看这里](https://lbsyun.baidu.com/index.php?title=jspopularGL/guide/custom)
-  第三步  将AK 和样式文件ID填入Axure 中继器数据中
![img.png](img.png)
-  第四部  预览文件
- 进阶 尝试修改百度地图增加其他个性化功能
- 修改大小请双击后拖动
![img_1.png](img_1.png)
- 中心点坐标 和级别设定 [请到这里获取相关坐标和级别数据](https://api.map.baidu.com/lbsapi/getpoint/index.html)