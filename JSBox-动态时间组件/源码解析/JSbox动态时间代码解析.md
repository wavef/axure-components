```
//引入day.js
// import https://cdn.jsdelivr.net/npm/dayjs@1/dayjs.min.js

THIS.$view.append(THIS.getUserElements())
updateTime()
//设置一秒执行一次 去除此行代码时间将不会变动
setInterval(updateTime, 1000)

function updateTime() {
    //时间格式可以调整 详情到day.js去找相对应的格式
  THIS.$view.find('div[data-label="Timer"] .text').text(dayjs().format("YYYY-MM-DD HH:mm:ss"));
}
```